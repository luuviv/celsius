package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

/*
 * @author Vivian Luu, 991558427
 * 
 * Class Description: This class will take a value in as Fahrenheit degress and returns it in Celsius degrees.
 * 
 * This class will be created and developed using TDD
 * 
 * */

public class CelsiusTest {

	//Regular
	@Test
	public void testFromFahrenheit() {
		int celcius = Celsius.fromFahrenheit(265);
		assertTrue("Incorrect temperature reading", celcius == 129);
	}
	
	//Exception
	@Test
	public void testFromFahrenheitException() {
		
		int celcius = Celsius.fromFahrenheit(2654);
		
		assertFalse("Incorrect temperature reading", celcius == 129);
	}
	
	//Boundary In
	@Test
	public void testFromFahrenheitBoundaryIn() {
		
		int celcius = Celsius.fromFahrenheit(30);
		
		assertTrue("Incorrect temperature reading", celcius == -1);
	}
	
	//Boundary Out
	@Test(expected = AssertionError.class)
	public void testFromFahrenheitBoundaryOut() {
		
		int celcius = Celsius.fromFahrenheit(123000);
		
		fail("Invalid temperature");
	}
}
