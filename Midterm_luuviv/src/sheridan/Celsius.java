package sheridan;

import java.util.Scanner;

/*
 * @author Vivian Luu, 991558427
 * 
 * Class Description: This class will take a value in as Fahrenheit degress and returns it in Celsius degrees.
 * 
 * This class will be created and developed using TDD
 * 
 * */

public class Celsius {

	public static void main(String arg[]) {
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Enter a tempearture in Fahrenheit: ");
		
		int F = in.nextInt();
		
		
		System.out.println(F + " degrees Fahrenheit is equal to " + fromFahrenheit(F) + " Celsius degrees");
	}
	
	public static int fromFahrenheit (int value) throws NumberFormatException{
	
		int celcius = ((5* (value - 32))/9);
		
		if (celcius >= 1000000) {
			throw new NumberFormatException("Number of milliseconds exceed range.");
		}
		
	return celcius;
	
	}

}
